<?php

namespace App\Service;

use App\Entity\Notification;
use App\Entity\NotificationTemplate;
use App\Entity\User;
use App\Factory\NotificationFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Response;

class NotificationManager
{
    private NotificationFactory $notificationFactory;
    private MailBuilder $mailBuilder;
    private EntityManagerInterface $em;

    public function __construct(NotificationFactory $notificationFactory, MailBuilder $mailBuilder, EntityManagerInterface $em)
    {
        $this->notificationFactory = $notificationFactory;
        $this->mailBuilder = $mailBuilder;
        $this->em = $em;
    }

    public function createAndSendMail(User $user, string $flag, array $additionalData = [], array $files = [])
    {
        $templatedEmail = $this->createMail($user, $flag, $additionalData, $files);
        $response = $this->sendMail($templatedEmail);

        return $response;
    }

    public function createMail(User $user, string $flag, array $additionalData = [], array $files = []): TemplatedEmail
    {
        /** @var NotificationTemplate */
        $notificationTemplate = $this->em->getRepository(NotificationTemplate::class)->findOneBy(['slug' => $flag]);
        $templatedEmail =
            $this->mailBuilder->createMail(
                $notificationTemplate->getFromEmail(),
                $notificationTemplate->getFromName(),
                $user->getEmail(),
                $user->getEmail(),
                $notificationTemplate->getSubject(),
                $notificationTemplate->getTwig(),
                $files,
                $additionalData,
                $notificationTemplate->getCopyAdmin()
            );

        return $templatedEmail;
    }

    public function sendMail(TemplatedEmail $email)
    {
        return $this->mailBuilder->sendMail($email);
    }

    public function sendSMS()
    {
        // TODO
        $response = new Response();

        return $response;
    }

    public function createNotification(User $user, string $flag, array $additionalData, string $type, $response = null)
    {
        $this->notificationFactory->createNotification($user, $flag, $additionalData, $type, $response);
    }

    public function createNotificationAndSend(User $user, string $flag, array $additionalData = [], array $files = [], $type = Notification::TYPE_EMAIL)
    {
        if ($type === Notification::TYPE_EMAIL) {
            $response = $this->createAndSendMail($user, $flag, $additionalData, $files);
        } else {
            $response = $this->sendSMS($user, $flag, $additionalData);
        }

        $this->createNotification($user, $flag, $additionalData, $type, $response);
    }
}
