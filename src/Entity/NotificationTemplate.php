<?php

namespace App\Entity;

use App\Repository\NotificationTemplateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="mail__template")
 * @ORM\Entity(repositoryClass=NotificationTemplateRepository::class)
 */
class NotificationTemplate
{
    public const NEW_ACCOUNT = 'new_account';
    public const CONFIRM_ACCOUNT = 'confirm_account';
    public const RESET_PASSWORD = 'reset_password';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $twig;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fromName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fromEmail;

    /**
     * @ORM\Column(type="boolean")
     */
    private $copyAdmin;

    public function getId(): int
    {
        return $this->id;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getTwig(): string
    {
        return $this->twig;
    }

    public function setTwig(string $twig): self
    {
        $this->twig = $twig;

        return $this;
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }

    public function setFromName(string $fromName): self
    {
        $this->fromName = $fromName;

        return $this;
    }

    public function getFromEmail(): string
    {
        return $this->fromEmail;
    }

    public function setFromEmail(string $fromEmail): self
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    public function getCopyAdmin(): bool
    {
        return $this->copyAdmin;
    }

    public function setCopyAdmin(bool $copyAdmin): self
    {
        $this->copyAdmin = $copyAdmin;

        return $this;
    }
}
