<?php

namespace App\Factory;

use App\Entity\Notification;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class NotificationFactory
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function createNotification(User $user, string $flag, array $additionalData, string $type, $response)
    {
        $notification = new Notification();
        $notification->setFlag($flag);
        $notification->setName($user->getEmail());
        $notification->setRecipient($user->getEmail());
        $notification->setType($type);
        if (!empty($additionalData)) {
            $notification->setAdditionalData($additionalData);
        }
        if ($response instanceof TemplatedEmail) {
            $notification->setStatus(Notification::STATUS_SENT_SUCCESSFUL);
        } elseif ($response === null) {
            $notification->setStatus(Notification::STATUS_TO_SEND);
        } else {
            $notification->setStatus(Notification::STATUS_SENT_BUT_ERROR);
        }
        $this->em->persist($notification);
        $this->em->flush();
    }
}
