<?php

namespace App\Controller\Admin;

use App\Entity\NotificationTemplate;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class NotificationTemplateCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NotificationTemplate::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('slug')->setDisabled(),
            TextField::new('subject'),
            TextField::new('twig')->setDisabled(),
            TextField::new('fromName'),
            TextField::new('fromEmail'),
            BooleanField::new('copyAdmin'),
        ];
    }
}
